var db = firebase.firestore();
var type = sessionStorage.getItem("type")
var userid = sessionStorage.getItem("userid")

db.collection("newcourse").get().then((snap) => {
  var data = []
  snap.forEach((doc) => {
    if (doc.data() !== undefined) {
      data.push(doc.data())
      document.getElementById('slider-container').innerHTML +=
        `
        <div class="col-lg-4 col-md-6 portfolio-item newcourse1">
        <div class="portfolio-wrap courseimgdiv" id="${doc.data().title}" onclick='view(this.id)'  style="background-image: url(${doc.data().thum});width:340px">
          <div class="portfolio-info">
            <h4>Rs. ${doc.data().ofees} <span style="text-decoration: line-through; font-weight: 400; color: rgba(255, 255, 255, 0.671)">Rs. ${doc.data().rfees}</span></h4>
          </div>
          <div style="color: white;
          font-weight: bold;
          margin-bottom: 60px;
          font-size: 19px;margin-top:20px;padding:10px;">${doc.data().title}</div>
        </div>
      </div>
      `
    }

  })
  if (data.length > 9) {
    var items = $("#slider-container .newcourse1");
    var numItems = data.length;
    var perPage = 9;
    items.slice(perPage).hide();
    $('#pagination-container1').pagination({
      items: numItems,
      itemsOnPage: perPage,
      prevText: "&laquo;",
      nextText: "&raquo;",
      onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        items.hide().slice(showFrom, showTo).show();
      }
    });

  }
})

view = (e) => {
  // if (userid == null) return toastr["info"]("Please Login And Continue...");
  sessionStorage.removeItem("categoryid")
  sessionStorage.setItem("courseid", e)
  window.location.replace("/dashboard/")
}

viewall = () => {
  sessionStorage.removeItem("categoryid")
  sessionStorage.removeItem("courseid")
  window.location.replace("/dashboard/")
}
const findcourse = document.getElementById("findcourse")
findcourse.addEventListener("keyup", async () => {
  var searchcourse = findcourse.value
  var searchcourses = document.getElementById("searchcourses")
  if (searchcourse.length == 0) return searchcourses.style.display = "none"
  searchcourses.style.display = "block"
  const allcourse = await allCourse()
  const filtercourse = await filterCourse(searchcourse, allcourse)
  const removeduplicates = await removeduplicate(filtercourse)
  document.getElementById("searchcourses").innerHTML = ""
  if (removeduplicates.length == 0) {
    document.getElementById("searchcourses").innerHTML =
      `
  <div class="searchitem" style="cursor: pointer;padding:10px;" >No Result Founded</div>
  `
  }
  removeduplicates.forEach((data) => {
    console.log(data)
    document.getElementById("searchcourses").innerHTML +=
      `
    <div class="searchitem" id="${data.course}" onclick="searchedcourse(this)" style="cursor: pointer;padding:10px;" >${data.course}</div>
    `
  })
})

removeduplicate = async (filtercourse) => {
  const removeduplicate = new Promise(async (resolve, reject) => {
    const filteredArr = filtercourse.reduce((acc, current) => {
      const x = acc.find(item => item.id === current.id);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    return resolve(filteredArr)
  })
  return await removeduplicate
}

allCourse = async () => {
  const allCourse = new Promise(async (resolve, reject) => {
    await db.collection("newcourse").get().then((snap) => {
      const data = []
      snap.forEach((doc) => {
        if (doc.data() != undefined) {
          data.push({ course: doc.data().title, id: doc.id })
        }
      })
      return resolve(data)
    })
  })
  return await allCourse
}

filterCourse = async (searchcourse, allcourse) => {
  const filterCourse = new Promise(async (resolve, reject) => {
    const data = []
    for (var i = 0; i < allcourse.length; i++) {
      var coures = await allcourse[i].course.toLowerCase()
      if (coures.includes(searchcourse.toLowerCase())) {
        data.push(allcourse[i])
      }
    }
    return resolve(data)
  })
  return await filterCourse
}

searchedcourse = (e) => {
  // if (userid == null) return toastr["info"]("Please Login And Continue...");
  sessionStorage.setItem("courseid", e.id)
  window.location.replace("/dashboard")
}
// coursefilter(All)


function coursefilter(e) {
  var category = sessionStorage.getItem("categorylist")
  if (category != null) {
    document.getElementById(`${category}`).style.backgroundColor = "white"
    document.getElementById(`${category}`).style.color = "#035096"
  }
  document.getElementById(`${e.id}`).style.backgroundColor = "#035096"
  document.getElementById(`${e.id}`).style.color = "white"
  setTimeout(() => {
    sessionStorage.setItem("categorylist", e.id)
  }, 2000)
  document.getElementById("pagination-container1").style.display = "none"
  var type = e.id
  if (type == "All") {
    document.getElementById('slider-container').innerHTML = ""
    db.collection("newcourse").get().then((snap) => {
      var data = []
      snap.forEach((doc) => {
        if (doc.data() !== undefined) {
          data.push(doc.data())
          document.getElementById('slider-container').innerHTML +=
            `
            <div class="col-lg-4 col-md-6 portfolio-item newcourse1">
            <div class="portfolio-wrap courseimgdiv" id="${doc.data().title}" onclick='view(this.id)'  style="background-image: url(${doc.data().thum});width:340px">
              <div class="portfolio-info">

                <h4>Rs. ${doc.data().ofees} <span style="text-decoration: line-through; font-weight: 400; color: rgba(255, 255, 255, 0.671)">Rs. ${doc.data().rfees}</span></h4>
              </div>
              <div style="color: white;
              font-weight: bold;
              margin-bottom: 60px;
              font-size: 19px;margin-top:20px;padding:10px;">${doc.data().title}</div>
            </div>
          </div>
          `
        }

      })
      if (data.length > 9) {
        var items = $("#slider-container .newcourse1");
        var numItems = data.length;
        var perPage = 9;
        items.slice(perPage).hide();
        $('#pagination-container1').pagination({
          items: numItems,
          itemsOnPage: perPage,
          prevText: "&laquo;",
          nextText: "&raquo;",
          onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
          }
        });

      }
    })
  } else {
    document.getElementById('slider-container').innerHTML = ""
    db.collection("newcourse").where("category", "==", type).get().then((snap) => {
      var data = [], dataid = []
      snap.forEach((doc) => {
        if (doc.data() != undefined) {
          data.push(doc.data())
          dataid.push(doc.id)
        }

      })


      for (var i = 0; i < data.length; i++) {
        if (data[i] != undefined) {
          document.getElementById('slider-container').innerHTML +=
            `  
            <div class="col-lg-4 col-md-6 portfolio-item newcourse1">
            <div class="portfolio-wrap courseimgdiv" id="${data[i].title}" onclick='view(this.id)'  style="background-image: url(${data[i].thum});width:340px">
              <div class="portfolio-info">
             
                <h4>Rs. ${data[i].ofees} <span style="text-decoration: line-through; font-weight: 400; color: rgba(255, 255, 255, 0.671)">Rs. ${data[i].rfees}</span></h4>
              </div>
              <div style="color: white;
              font-weight: bold;
              margin-bottom: 60px;
              font-size: 19px;margin-top:20px;padding:10px;">${data[i].title}</div>
            </div>
          </div>
          `
        }
      }
      if (data.length > 9) {
        document.getElementById("pagination-container1").style.display = "block"
        var items = $("#slider-container .newcourse1");
        var numItems = data.length;
        var perPage = 9;
        items.slice(perPage).hide();
        $('#pagination-container1').pagination({
          items: numItems,
          itemsOnPage: perPage,
          prevText: "&laquo;",
          nextText: "&raquo;",
          onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
          }
        });

      }
    })
  }

}



db.collection("users").get().then(async (snap) => {
  var sendusers = []
  var today = moment().format("MM-DD")
  snap.forEach(async (doc) => {
    var dobday = moment(doc.data().dob).format("MM-DD")
    if (dobday === today) {
      if (doc.data().birthday !== "true") {
        const username = `${doc.data().firstname} ${doc.data().lastname}`
        const sendemail = await sendEamil(doc.data().email, username)
        if (sendemail === true) {
          await db.collection("users").doc(doc.data().clientid).update({
            birthday: "true",
            date: today
          }).then(() => { console.log("okk") })
        }
      } else if (doc.data().birthday === "true" && doc.data().date !== today) {
        const username = `${doc.data().firstname} ${doc.data().lastname}`
        const sendemail = await sendEamil(doc.data().email, username)
        if (sendemail === true) {
          await db.collection("users").doc(doc.data().clientid).update({
            birthday: "true",
            date: today
          }).then(() => { console.log("okk") })
        }
      }


    }
  })
})

sendEamil = async (email, username) => {
  document.getElementById("emailusername").innerHTML = username
  const mailtemplate = await document.getElementById("logintemplate").innerHTML
  const sendmail = await axios.post("/mail/dob", {
    email: email,
    mailtemplate: mailtemplate
  }).then((res) => {
    return res.data
  }).catch((error) => {
    console.log(error)
    return false
  })
  return sendmail
}

console.log("first")