const router = require("express").Router()
var nodemailer = require('nodemailer')


var smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'noreply@cmcacademy.ac.in',
        pass: 'welcome@2022' // naturally, replace both with your real credentials or an application-specific password
    }
});


router.post('/registersend', async (req, res) => {
    const mailOptions = {
        from: 'noreply@cmcacademy.ac.in',
        to: req.body.email,
        subject: 'Account Registation..',
        html: req.body.mailtemplate
    };

    smtpTransport.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            return res.send(true)
        }
    });
})

router.post('/login', async (req, res) => {
    const mailOptions = {
        from: 'noreply@cmcacademy.ac.in',
        to: req.body.email,
        subject: 'Account Login..',
        html: req.body.mailtemplate
    };

    smtpTransport.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            return res.send(true)
        }
    });
})

router.post('/dob', async (req, res) => {
    const mailOptions = {
        from: 'noreply@cmcacademy.ac.in',
        to: req.body.email,
        subject: 'Birthday Wishes',
        html: req.body.mailtemplate
    };

    smtpTransport.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            return res.send(true)
        }
    });
})

router.post('/coursepurchasesend', async (req, res) => {
    mailOptions = {
        from: 'notification@merchantox.com',
        to: req.body.email,
        subject: "Course Purchase",
        html: req.body.mailtemplate,
    }
    smtpTransport.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            return res.send(true)
        }
    });
})



module.exports = router