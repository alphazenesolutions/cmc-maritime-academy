var db = firebase.firestore();
db.collection("announcement").doc("msg").get().then((doc) => {
    if (doc.data() != undefined) {
        document.getElementById("announcement").innerHTML += `
        <div class="d-flex container p-2 align-items-center">
            <span class="mr-4">Announcement</span>
            <marquee behavior="scroll" direction="left" >
              ${doc.data().announcement}
            </marquee>
          </div>
        `
    }
    else {
        document.getElementById("announcement").innerHTML += `
        <div class="d-flex container p-2 align-items-center">
            <span class="mr-4">Announcement</span>
            <marquee behavior="scroll" direction="left" >
            For admission or enquiries, contact Telephone: +91 44
            42691188 / 42651199 / 26431188 / 26431199.
            Mobile: +91 98436 55985.
            </marquee>
          </div>
        `
    }
})