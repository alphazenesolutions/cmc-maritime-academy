var db = firebase.firestore();
db.collection("testimonial").get().then((snap) => {
  snap.forEach((doc) => {
    db.collection("users").doc(doc.data().userid).get().then((docs) => {
      console.log(docs.data());
      document.getElementById("feedbackview").innerHTML += `
      <div class="col-lg-6">
          <div class="testimonial-item">
            <img class="testimonial-img" src="${docs.data().profilepic == undefined ? "/img/testimonials/testimonials-1.jpg  " : docs.data().profilepic}" alt="">
            <h3>${doc.data().name}</h3>
            <h4>Student</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              ${doc.data().testimonial}
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>
        </div>`
    })

  })
})


// db.collection("testimonial").limit(4).get().then((snaps) => {
//   snaps.forEach((docs) => {
//     document.getElementById("testimonialview").innerHTML += `

//           <div class="item">
//             <div class="testimonial-item">
//               <img src="/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
//               <h3>${docs.data().name}</h3>
//               <h4>Ceo &amp; Founder</h4>
//               <p>
//                 <i class="bx bxs-quote-alt-left quote-icon-left"></i>
//                 ${docs.data().testimonial}
//                 <i class="bx bxs-quote-alt-right quote-icon-right"></i>
//               </p>
//             </div>
//           </div>`
//   })
// })