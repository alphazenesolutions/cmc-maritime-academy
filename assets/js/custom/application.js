var userid = sessionStorage.getItem("userid");
document
  .getElementById("applicationreg")
  .addEventListener("click", async () => {
    var place = document.getElementById("place").value;
    var state = document.getElementById("state").value;
    var country = document.getElementById("country").value;
    var address = document.getElementById("address").value;
    var city = document.getElementById("city").value;
    var pincode = document.getElementById("pincode").value;
    var passport = document.getElementById("passport").value;
    var discharge = document.getElementById("discharge").value;
    var grade = document.getElementById("grade").value;
    var gradenumber = document.getElementById("gradenumber").value;
    var year = document.getElementById("year").value;
    var month = document.getElementById("month").value;
    var day = document.getElementById("day").value;
    var indosnumber = document.getElementById("indosnumber").value;
    var bsidnumber = document.getElementById("bsidnumber").value;
    var dischargeimg = document.querySelector("#dischargeimg").files;
    var gradeimg = document.querySelector("#gradeimg").files;
    var medicalimg = document.querySelector("#medicalimg").files;
    var passportimg = document.querySelector("#passportimg").files;
    var previouscertificateimg = document.querySelector(
      "#previouscertificateimg"
    ).files;
    var signatureimg = document.querySelector("#signatureimg").files;
    var BSIDdiv = document.getElementById("BSIDdiv").style.display;
    if (BSIDdiv == "block") {
      var basidreg = /[A-Z]{1}([0-9]){8}/;
      if (bsidnumber.length == 0) {
        document.getElementById("bsiderror").innerHTML =
          "Biometric Seafarers Identity Document (BSID) Required";
      } else if (basidreg.test(bsidnumber) == false) {
        document.getElementById("bsiderror").innerHTML =
          "Biometric Seafarers Identity Document (BSID) Must Be this format A00000000";
      } else {
        document.getElementById("bsiderror").innerHTML = " ";
        document.getElementById("applicationreg").disabled = true;
        toastr["info"]("Please Wait....");
        document.getElementById("applicationreg").innerHTML =
          " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait...";
        var disurl = await uploadImage(dischargeimg[0]);
        var medicalurl = await uploadImage(medicalimg[0]);
        var certificateurl = await uploadImage(previouscertificateimg[0]);
        var signatureurl = await uploadImage(signatureimg[0]);
        var gradeimgurl = await uploadImage(gradeimg[0]);
        var passportimgurl = await uploadImage(passportimg[0]);
        var data = {
          place: place,
          state: state,
          country: country,
          address: address,
          pincode: pincode,
          city: city,
          passport: passport,
          discharge: discharge,
          indosnumberapplication: indosnumber,
          dischargeurl: disurl,
          medicalurl: medicalurl,
          certificateurl: certificateurl,
          signatureurl: signatureurl,
          clientid: userid,
          gradeimgurl: gradeimgurl,
          passportimgurl: passportimgurl,
          grade: grade,
          gradenumber: gradenumber,
          year: year,
          month: month,
          day: day,
          application: true,
          bsidnumber: bsidnumber,
        };
        var userdata = await axios
          .post("/updateapplication", data)
          .then((res) => {
            return res.data;
          });
        if (userdata == true) {
          toastr["success"]("Application Updated Successfully..");
          setTimeout(() => {
            getdata();
          }, 2000);
        }
        getdata();
      }
    } else {
      document.getElementById("bsiderror").innerHTML = " ";
      document.getElementById("applicationreg").disabled = true;
      toastr["info"]("Please Wait....");
      document.getElementById("applicationreg").innerHTML =
        " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait...";
      var disurl = await uploadImage(dischargeimg[0]);
      var medicalurl = await uploadImage(medicalimg[0]);
      var certificateurl = await uploadImage(previouscertificateimg[0]);
      var signatureurl = await uploadImage(signatureimg[0]);
      var gradeimgurl = await uploadImage(gradeimg[0]);
      var passportimgurl = await uploadImage(passportimg[0]);
      var data = {
        place: place,
        state: state,
        country: country,
        address: address,
        pincode: pincode,
        city: city,
        passport: passport,
        discharge: discharge,
        indosnumberapplication: indosnumber,
        dischargeurl: disurl,
        medicalurl: medicalurl,
        certificateurl: certificateurl,
        signatureurl: signatureurl,
        clientid: userid,
        gradeimgurl: gradeimgurl,
        passportimgurl: passportimgurl,
        grade: grade,
        gradenumber: gradenumber,
        year: year,
        month: month,
        day: day,
        application: true,
        bsidnumber: bsidnumber,
      };
      var userdata = await axios
        .post("/updateapplication", data)
        .then((res) => {
          return res.data;
        });
      if (userdata == true) {
        toastr["success"]("Application Updated Successfully..");
        setTimeout(() => {
          getdata();
        }, 2000);
      }
      getdata();
    }
  });

async function uploadImage(file) {
  const image = new Promise((resolve, reject) => {
    var path = `application/${userid}/`;
    var fileName = `${Date.now().toString()}`;
    var storageRef = firebase.storage().ref(path + fileName);
    storageRef.put(file).then(function (snapshot) {
      storageRef.getDownloadURL().then(function (url) {
        resolve(url);
      });
    });
  });

  const imageURL = await image;
  return imageURL;
}

async function getdata() {
  var totalcheckout = sessionStorage.getItem("totalamount");
  const mycartsitem = sessionStorage.getItem("mycartsitems");
  const mycardid = [sessionStorage.getItem("mycardid")];

  toastr["info"]("Please Wait");
  var url = "/razorpay/payment";
  const razorpay = await axios
    .post(
      url,
      {
        amount: totalcheckout,
      },
      {
        headers: {
          clientuserid: userid,
        },
      }
    )
    .then((res) => {
      if (res.data.status == "success") {
        return res.data.sub.id;
      } else {
        return false;
      }
    })
    .catch((error) => {
      return false;
    });
  if (razorpay == false) return toastr["error"]("Something Wrong");
  const payamount = await pay(razorpay);
  if (payamount !== undefined) {
    toastr["info"]("Please Wait");
    const updatemycourse = await mycourse(
      mycartsitem,
      payamount,
      mycardid,
      totalcheckout
    );
    if (updatemycourse === true) {
      toastr["success"]("Course Purchased Succesfully..");
      setTimeout(() => {
        return window.location.replace("/dashboard/mycourses");
      }, 2000);
    } else {
      toastr["error"]("Try Again..");
    }
  } else {
    alert("Please Try Again..");
  }
}

pay = async (payid) => {
  const pay = new Promise(async (resolve, reject) => {
    var options = {
      key: "rzp_live_MVRHbra47SbyjW", //Enter your razorpay key
      currency: "INR",
      name: "CMC MARITIME ACADEMY",
      description: "Course Purchase",
      image: "/img/logo/cmc_logo.png",
      order_id: payid,
      handler: function (response) {
        return resolve(response.razorpay_payment_id);
      },
      theme: {
        color: "#227254",
      },
    };
    var rzp = new Razorpay(options);
    rzp.open();
  });
  return await pay;
};

mycourse = async (itemlist, payamount, mycardid, amount) => {
  var mycartsitem = itemlist.split(",");
  const mycourse = new Promise(async (resolve, reject) => {
    const status = [];
    for (var i = 0; i < mycartsitem.length; i++) {
      const date = new Date();
      const monthsandday = [
        "00",
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23",
        "24",
        "25",
        "26",
        "27",
        "28",
        "29",
        "30",
        "31",
        "32",
        "33",
        "34",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42",
        "43",
        "44",
        "45",
        "46",
        "47",
        "48",
        "49",
        "50",
        "51",
        "52",
        "53",
        "54",
        "55",
        "56",
        "57",
        "58",
        "59",
        "60",
      ];
      const day =
        date.getFullYear() +
        "-" +
        monthsandday[date.getMonth() + 1] +
        "-" +
        date.getDate();
      var time =
        monthsandday[date.getHours()] +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds();
      const today = day + " " + time;
      const mycarts = await db
        .collection("cart")
        .doc(mycartsitem[i])
        .get()
        .then(async (docss) => {
          if (docss.data() !== undefined) {
            return docss.data();
          }
        });
      if (mycarts !== undefined) {
        var purchasecourses = {
          userid: userid,
          coureseid: mycarts.courseid,
          date: moment().format("YYYY-MM-DD"),
          amount: Number(mycarts.fees),
          paymentid: payamount,
          status: "Not Started",
          batch: mycarts.batch,
          batchdate: mycarts.startdate,
        };
        await db
          .collection("purchasecourses")
          .doc()
          .set(purchasecourses)
          .then((doc) => {
            return doc.data();
          })
          .catch((error) => {
            status.push(true);
          });
        const coursedata = await db
          .collection("newcourse")
          .doc(mycarts.courseid)
          .get()
          .then(async (docs) => {
            if (docs.data() !== undefined) {
              return docs.data();
            }
          });
        const userinfo = await db.collection("users").doc(userid);
        const myinfo = await userinfo.get().then(async (doc) => {
          if (doc.data() != undefined) {
            return doc.data();
          }
        });
        const myinfoupdate = await userinfo
          .update({
            purchasecourses: [mycartsitem[i]],
          })
          .then(() => {
            return true;
          })
          .catch((error) => {
            return false;
          });
        if (myinfoupdate === true) {
          status.push(true);
        }

        const sendEmail = await sendmail(coursedata, myinfo);
        const removecart = await removeMycart(mycartsitem[i]);
      } else {
        alert("Something went wrong..");
      }
    }
    if (status.includes(false)) return resolve(false);
    return resolve(true);
  });
  return await mycourse;
};

sendmail = async (data, userdata) => {
  var { email } = userdata;
  var { title } = data;
  document.getElementById("mailcoursename").innerHTML = title;
  const mailtemplate = await document.getElementById("purchasetemplate")
    .innerHTML;
  const sendmail = await axios
    .post("/mail/coursepurchasesend", {
      email: email,
      mailtemplate: mailtemplate,
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      return false;
    });
  return sendmail;
};

removeMycart = async (mycartsitem) => {
  // for (var i = 0; i < mycartsitem.length; i++) {
  const removeMycart = new Promise(async (resolve, reject) => {
    await db
      .collection("cart")
      .doc(mycartsitem)
      .delete()
      .then(() => {
        return resolve(true);
      })
      .catch((error) => {
        return resolve(false);
      });
  });

  // }
};

var userid = sessionStorage.getItem("userid");
db.collection("users")
  .doc(userid)
  .get()
  .then((doc) => {
    document.getElementById("place").value = `${
      doc.data().place == undefined ? "" : doc.data().place
    }`;
    document.getElementById("state").value = `${
      doc.data().state == undefined ? "" : doc.data().state
    }`;
    document.getElementById("country").value = `${
      doc.data().country == undefined ? "" : doc.data().country
    }`;
    document.getElementById("address").value = `${
      doc.data().address == undefined ? "" : doc.data().address
    }`;
    document.getElementById("city").value = `${
      doc.data().city == undefined ? "" : doc.data().city
    }`;
    document.getElementById("pincode").value = `${
      doc.data().pincode == undefined ? "" : doc.data().pincode
    }`;
    document.getElementById("passport").value = `${
      doc.data().passport == undefined ? "" : doc.data().passport
    }`;
    document.getElementById("discharge").value = `${
      doc.data().discharge == undefined ? "" : doc.data().discharge
    }`;
    document.getElementById("grade").value = `${
      doc.data().grade == undefined ? "" : doc.data().grade
    }`;
    document.getElementById("gradenumber").value = `${
      doc.data().gradenumber == undefined ? "" : doc.data().gradenumber
    }`;
    document.getElementById("year").value = `${
      doc.data().year == undefined ? "" : doc.data().year
    }`;
    document.getElementById("month").value = `${
      doc.data().month == undefined ? "" : doc.data().month
    }`;
    document.getElementById("day").value = `${
      doc.data().day == undefined ? "" : doc.data().day
    }`;
    document.getElementById("indosnumber").value = `${
      doc.data().indosnumber == undefined ? "" : doc.data().indosnumber
    }`;
  });

db.collection("users")
  .doc(userid)
  .get()
  .then((doc) => {
    if (doc.data() != undefined) {
      if (doc.data().profilepic !== undefined) {
        document.getElementById("dashdppic").src = `${doc.data().profilepic}`;
      }
    }
  });
