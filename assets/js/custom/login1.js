const loginbtn = document.getElementById("loginbtn")
var db = firebase.firestore()
loginbtn.addEventListener("click", async () => {
    const indosno = document.getElementById("indosno").value
    const password = document.getElementById("password").value

    if (indosno.trim().length == 0) {
    } else if (password.trim().length == 0) {

    } else {
        document.getElementById("loginbtn").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
        const accCheck = await axios.get("/check", {
            headers: {
                indosnumber: indosno
            }
        }).then((res) => {
            return res.data
        }).catch((error) => {
            console.log(error)
            return false
        })
        if (accCheck == false) {
            toastr["error"]("Not Registered..")
            setTimeout(() => {
                window.location.reload()
            }, 2000)
        }

        if (accCheck.length != 1) return toastr["error"]("Something Wrong..")
        const pwdverify = await axios.post("/hash/verify", {
            password: password,
            hashPwd: accCheck[0].password
        }).then((res) => { return res.data }).catch((error) => { return false })
        if (pwdverify != false) {
            const loginuser = await new Promise(async (resolve, reject) => {
                await firebase.auth().signInWithEmailAndPassword(accCheck[0].email, password).then(async function () {
                    await firebase.auth().onAuthStateChanged(function (user) {
                        if (user) {
                            return resolve({ status: true, clientid: user.uid })
                        } else {
                            return resolve({ status: false, msg: "Try Again" })
                        }
                    });
                }).catch(function (error) {
                    var errorMessage = error.message;
                    return resolve({ status: false, msg: errorMessage })
                });
            })
            if (loginuser.status == false) return toastr["error"](loginuser.msg)
            db.collection("users").doc(loginuser.clientid).get().then(async(docss) => {
                if (docss.data().application == true) {
                    toastr["success"]("Welcome To CMC Maritime-Academy..");
                    const username = `${docss.data().firstname} ${docss.data().lastname}`
                    sendEamil(docss.data().email, username)
                    sessionStorage.setItem("userid", loginuser.clientid)
                    setTimeout(() => {
                        window.location.replace("/")
                    }, 1000)
                } else {
                    toastr["info"]("Welcome To CMC Maritime-Academy..");
                    console.log(loginuser)
                    sessionStorage.setItem("userid", loginuser.clientid)
                    const username = `${docss.data().firstname} ${docss.data().lastname}`
                    sendEamil(docss.data().email, username)
                    setTimeout(() => {
                        window.location.replace("/")
                    }, 1000)
                }
            })
        } else {
            const loginuser = await new Promise(async (resolve, reject) => {
                await firebase.auth().signInWithEmailAndPassword(accCheck[0].email, password).then(async function () {
                    await firebase.auth().onAuthStateChanged(function (user) {
                        if (user) {
                            return resolve({ status: true, clientid: user.uid })
                        } else {
                            return resolve({ status: false, msg: "Try Again" })
                        }
                    });
                }).catch(function (error) {
                    var errorMessage = error.message;
                    return resolve({ status: false, msg: errorMessage })
                });
            })
            if (loginuser.status == false) return toastr["error"](loginuser.msg)
            const hashpwd = await axios.post("/hash/generate", {
                password: password
            }).then((res) => {
                return res.data
            }).catch((error) => {
                return false
            })
            const newHashpwd = {
                password: hashpwd
            }
            const updatePwd = await axios.post("/updatepassword", newHashpwd, {
                headers: {
                    clientid: loginuser.clientid
                }
            }).then((res) => { return res.data }).catch((error) => { return false })
            if (updatePwd == true) {

                db.collection("users").doc(loginuser.clientid).get().then(async(docss) => {
                    if (docss.data().application == true) {
                        toastr["success"]("Welcome To CMC Maritime-Academy..");
                        sessionStorage.setItem("userid", loginuser.clientid)
                        const username = `${docss.data().firstname} ${docss.data().lastname}`
                        sendEamil(docss.data().email, username)
                        setTimeout(() => {
                            window.location.replace("/")
                        }, 1000)
                    } else {
                        toastr["info"]("Welcome To CMC Maritime-Academy..");
                        sessionStorage.setItem("userid", loginuser.clientid)
                        const username = `${docss.data().firstname} ${docss.data().lastname}`
                        sendEamil(docss.data().email, username)
                        setTimeout(() => {
                            window.location.replace("/")
                        }, 1000)
                    }
                })

            } else {
                toastr["error"]("Something Wrong Try Again")
                window.location.reload()
            }
        }

    }
})


sendEamil = async (email, username) => {
    document.getElementById("emailusername").innerHTML = username
    const mailtemplate = await document.getElementById("logintemplate").innerHTML
    const sendmail = await axios.post("/mail/login", {
        email: email,
        mailtemplate: mailtemplate
    }).then((res) => {
        return res.data
    }).catch((error) => {
        console.log(error)
        return false
    })
    return sendmail
}
