var db = firebase.firestore();
var userid = sessionStorage.getItem("userid")

document.getElementById("sendTestimonial").addEventListener("click", () => {
    var testimonial = document.getElementById("testimonial").value
    if (testimonial == 0) {
        document.getElementById("errordiv").innerHTML +=
            `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Please Provide Valid testimonial
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        `
    }
    else {
        document.getElementById("sendTestimonial").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
        db.collection("users").doc(userid).get().then((doc) => {
            db.collection("testimonial").doc().set({
                testimonial: testimonial,
                userid: userid,
                name: `${doc.data().firstname + " " + doc.data().lastname}`,
            }).then(() => {
                toastr["success"]("Testimonial Added Successfully...");
                setTimeout(() => {
                    window.location.reload()
                }, 1000)
            })
        })

    }





})

